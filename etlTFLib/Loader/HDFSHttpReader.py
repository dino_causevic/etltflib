import os
from etlTFLib.common.Logger import logger
from etlTFLib.common.FileSystem.HDFSHttp import HDFSHttp
from etlTFLib.common.ETLExceptions import UserWorkspaceNotExists, HDFSConnectionError, HDFSPathOutUserWorkspace
from etlTFLib.config import USER_WORKSPACE_TEMPLATE, HDFS_HTTP_HOST, HDFS_HTTP_PORT


class HDFSReader(object):

    def __init__(self, user_id=None):

        if not user_id:
            logger.error("User ID is not provided.")
            raise ValueError("User ID is not provided.")

        self.user_id = user_id
        try:
            self.hdfs_client = HDFSHttp(host=HDFS_HTTP_HOST, port=HDFS_HTTP_PORT)
        except HDFSConnectionError as cex:
            logger.exception(cex)
            raise HDFSConnectionError("Problem with connecting on HDFS.", cex)

        self.user_workspace = USER_WORKSPACE_TEMPLATE.format(self.user_id)

        if not self.hdfs_client.status(self.user_workspace):
            logger.error("User workspace doesn't exists.")
            raise UserWorkspaceNotExists("User workspace doesn't exists.")

    def read(self, file_path, encoding=None, chunk_size=None, delimiter=None):

        if self.user_workspace not in os.path.normpath(self.user_workspace + file_path):
            logger.error(
                f"Path {os.path.normpath(self.user_workspace + file_path)} is outside user workspace."
            )
            raise HDFSPathOutUserWorkspace(
                f"Path {os.path.normpath(self.user_workspace + file_path)} is outside user workspace."
            )

        return self.hdfs_client.read(os.path.normpath(self.user_workspace + file_path), encoding, chunk_size, delimiter)
