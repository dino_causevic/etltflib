from etlTFLib.common.ModelSaver.ModelLoaderAbstract import ModelLoaderAbstract


class HDFSModelLoader(ModelLoaderAbstract):

    def __init__(self, model_id, model_name, user_id):
        super(HDFSModelLoader, self).__init__(model_id, model_name, user_id)