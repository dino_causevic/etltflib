from etlTFLib.common.Logger import logger
from etlTFLib.common.FileSystem.HDFSNative import HDFSNative
from etlTFLib.common.ETLExceptions import UserWorkspaceNotExists, HDFSConnectionError
from etlTFLib.config import USER_WORKSPACE_TEMPLATE, HDFS_HOST, HDFS_PORT, HDFS_USER


class HDFSNativeReader(object):

    def __init__(self, user_id=None):
        if not user_id:
            logger.error("User ID is not provided.")
            raise ValueError("User ID is not provided.")

        self.user_id = user_id

        try:
            self.hdfs_client = HDFSNative(host=HDFS_HOST, port=HDFS_PORT, user=HDFS_USER)
        except HDFSConnectionError as cex:
            logger.exception(cex)
            raise HDFSConnectionError("Problem with connecting on HDFS.", cex)

        self.user_workspace = USER_WORKSPACE_TEMPLATE.format(self.user_id)

        if not self.hdfs_client.status(self.user_workspace):
            logger.error("User workspace doesn't exists.")
            raise UserWorkspaceNotExists("User workspace doesn't exists.")

    def read(self, file_path):
        return self.hdfs_client.open(file_path, mode="rb")
