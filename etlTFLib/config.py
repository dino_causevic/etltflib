import os
import socket

USER_WORKSPACE_TEMPLATE = "/user/root/{}"

HDFS_HTTP_HOST = "http://hadoop_hdfs"
HDFS_HTTP_PORT = 50070

HDFS_HOST = socket.gethostbyname("hadoop_hdfs")
HDFS_PORT = 9000
HDFS_USER = "root"

MODEL_DEFAULT_PATH = "/app/model/data/session"

HDFS_MODEL_PATH = os.environ.get(
    "TF_HDFS_MODEL_PATH",
    None
)
