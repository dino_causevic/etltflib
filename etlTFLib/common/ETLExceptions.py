class ETLAbstractException(Exception):

    def __init__(self, msg, reason=None):
        self._reason = reason
        super(ETLAbstractException, self).__init__(msg)

    @property
    def reason(self):
        return self._reason


class UserWorkspaceNotExists(ETLAbstractException):

    def __init__(self, msg=None, reason=None):
        super(UserWorkspaceNotExists, self).__init__(msg, reason)


class HDFSConnectionError(ETLAbstractException):

    def __init__(self, msg=None, reason=None):
        super(HDFSConnectionError, self).__init__(msg, reason)


class HDFSPathOutUserWorkspace(ETLAbstractException):

    def __init__(self, msg=None, reason=None):
        super(HDFSPathOutUserWorkspace, self).__init__(msg, reason)


class DirectoryNotFound(ETLAbstractException):

    def __init__(self, msg=None, reason=None):
        super(DirectoryNotFound, self).__init__(msg, reason)


class SessionGraphMissing(ETLAbstractException):

    def __init__(self, msg=None, reason=None):
        super(SessionGraphMissing, self).__init__(msg, reason)


class SessionSavingFailed(ETLAbstractException):

    def __init__(self, msg=None, reason=None):
        super(SessionSavingFailed, self).__init__(msg, reason)
