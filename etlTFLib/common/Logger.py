import logging

logger = logging.getLogger("ETL-TF-Lib")
logger.setLevel(logging.DEBUG)

stdout_log_stream = logging.StreamHandler()
stdout_log_stream.setLevel(logging.DEBUG)
stdout_log_stream.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))

logger.addHandler(stdout_log_stream)
