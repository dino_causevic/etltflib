import tensorflow as tf
import os
from etlTFLib.config import MODEL_DEFAULT_PATH, USER_WORKSPACE_TEMPLATE
from etlTFLib.common.FileSystem.HDFSHttp import HDFSHttp
from etlTFLib.common.Logger import logger


class ModelLoaderAbstract(object):

    def __init__(self, model_id, model_name, user_id, path=MODEL_DEFAULT_PATH):

        if not model_id:
            logger.error("Model ID has to be provided.")
            raise ValueError("Model ID has to be provided.")

        if not user_id:
            logger.error("User ID has to be provided.")
            raise ValueError("User ID has to be provided.")

        self.user_id = user_id
        self.model_id = model_id
        self.model_name = model_name
        self.path = path

        path_split = self.path.split("/")

        self.client = HDFSHttp()
        self.user_workspace = USER_WORKSPACE_TEMPLATE.format(self.user_id)
        self.model_path = os.path.normpath(self.user_workspace + "/models/" + self.model_id + "/" + self.model_name)

        self.client.download(self.model_path + "/train/data/", "/".join(path_split[:-2]))

        self._session = tf.Session()
        self.saver = tf.train.import_meta_graph(path + ".meta")
        self.saver.restore(self._session, tf.train.latest_checkpoint("/".join(path_split[:-1])))
        self._graph = tf.get_default_graph()

    @property
    def session(self):
        return self._session

    @property
    def graph(self):
        return self._graph

