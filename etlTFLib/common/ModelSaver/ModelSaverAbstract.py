import tensorflow as tf
from etlTFLib.common.ETLExceptions import DirectoryNotFound, SessionGraphMissing, SessionSavingFailed
from etlTFLib.common.Logger import logger


class ModelSaverAbstract(object):

    def __init__(self, session, path):

        if not path:
            logger.error("Path for model saver need to be provided.")
            raise DirectoryNotFound("Path for model saver need to be provided.")

        if not session:
            logger.error("Session graph missing")
            raise SessionGraphMissing("Session graph missing")

        self.session = session
        self.path = path
        self.saver = tf.train.Saver()

    def save(self):
        try:
            if not self.session._closed:
                logger.debug("Session open and save called.")
                return self.saver.save(self.session, self.path)
            else:
                logger.warning("Session closed saving can't be called.")

            return None
        except Exception as ex:
            logger.exception(ex)
            raise SessionSavingFailed("Problem with saving model.", reason=ex)


