import pyarrow as pa
from etlTFLib.config import HDFS_HOST, HDFS_PORT, HDFS_USER
from etlTFLib.common.Logger import logger
from etlTFLib.common.FileSystem.FileSystemAbstract import FileSystemAbstract
from etlTFLib.common.ETLExceptions import HDFSConnectionError


class HDFSNative(FileSystemAbstract):

    def __init__(self, host=HDFS_HOST, port=HDFS_PORT, user=HDFS_USER):

        if not host:
            raise ValueError("Host is not provided.")

        if not port:
            raise ValueError("Port is not provided.")

        if not user:
            raise ValueError("User is not provided.")

        super(HDFSNative, self).__init__()
        self.host = host
        self.port = port
        self.user = user

        try:
            self.client = pa.hdfs.connect(f"hdfs://{host}", port, user=user)
        except Exception as hcex:
            logger.exception(hcex)
            raise HDFSConnectionError(f"Problem with connecting on HDFS ({host}:{port}).", reason=hcex)

    def close(self):
        try:
            if self.client:
                self.client.close()
        except Exception as ex:
            logger.exception(ex)

    def ls(self, path=None, detail=False):
        logger.debug("List called.")
        return self.client.ls(path, detail=detail)

    def exists(self, path):
        return self.client.exists(path)

    def status(self, path, strict=False):
        return self.exists(path)

    def read(self, filepath):

        if not filepath:
            raise ValueError("File path is not provided.")

        if not self.exists(filepath):
            raise FileNotFoundError(f"File {filepath} doesn't exists.")

        try:
            return self.open(filepath, mode="rb")
        except Exception as ex:
            logger.exception(ex)

    def open(self, filepath, mode="rb"):
        return self.client.open(filepath, mode=mode)

    def rename(self, src_path, dst_path):
        logger.debug("Rename called.")
        return self.client.rename(src_path, dst_path)

    def delete(self, path, recursive=False):
        logger.debug("Delete called.")
        return self.client.delete(path, recursive=recursive)

    def upload(self, path, stream, buffer_size):
        logger.debug("Upload called.")
        return self.client.upload(path, stream, buffer_size=buffer_size)

    def info(self, path):
        logger.debug("Info called.")
        return self.client.info(path)

    def mkdir(self, path):
        logger.debug("Mkdir called.")
        return self.client.mkdir(path)
