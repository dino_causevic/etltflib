from etlTFLib.config import HDFS_HTTP_HOST, HDFS_HTTP_PORT
from etlTFLib.common.Logger import logger
from etlTFLib.common.FileSystem.FileSystemAbstract import FileSystemAbstract
from etlTFLib.common.ETLExceptions import HDFSConnectionError
from hdfs import InsecureClient


class HDFSHttp(FileSystemAbstract):

    def __init__(self, host=HDFS_HTTP_HOST, port=HDFS_HTTP_PORT):

        if not host:
            logger.error("Host need to be provided.")
            raise ValueError("Host need to be provided.")

        if not port or (port and port <= 0):
            logger.error("Correct port number need to be provided.")
            raise ValueError("Correct port number need to be provided.")

        self.host = host
        self.port = port

        try:
            self.client = InsecureClient(f"{host}:{port}", user="root")
            self.client.status('/')
        except Exception as ex:
            logger.exception(ex)
            raise HDFSConnectionError("Problem connecting on HDFS.", ex)

    def ls(self, path=None):
        logger.debug("HDFS ls called.")
        path = path or "/"
        return self.client.list(path)

    def rename(self, src_path, dst_path):
        logger.debug("HDFS rename called.")
        return self.client.rename(src_path, dst_path)

    def delete(self, path, recursive=False):
        logger.debug("HDFS delete called.")
        return self.client.delete(path, recursive=recursive)

    def status(self, path, strict=False):
        logger.debug("HDFS status called.")
        return self.client.status(path, strict=strict)

    def read(self, file_path, encoding=None, chunk_size=None, delimiter=None):
        logger.debug("HDFS read called.")
        params = {}

        if chunk_size:
            params['chunk_size'] = chunk_size

        if delimiter:
            params['delimiter'] = delimiter

        if encoding:
            params['encoding'] = encoding

        with self.client.read(file_path, **params) as reader:
            for data in reader:
                yield data

    def mkdir(self, path, permissions=777):
        logger.debug("HDFS mkdir called.")
        return self.client.makedirs(path, permissions)

    def upload(self, hdfs_path, local_path, overwrite=True, n_threads=4):
        logger.debug("HDFS upload called.")
        return self.client.upload(hdfs_path, local_path, overwrite, n_threads=n_threads)

    def download(self, hdfs_path, local_path, overwrite=True, n_threads=4):
        logger.debug("HDFS download called.")
        self.client.download(hdfs_path, local_path, overwrite=overwrite, n_threads=n_threads)
