from abc import abstractmethod


class FileSystemAbstract(object):

    @abstractmethod
    def ls(self, path=None):
        raise NotImplementedError("List files in folder is not implemented.")

    @abstractmethod
    def read(self, filepath):
        raise NotImplementedError("Read method is not implemented.")

    @abstractmethod
    def rename(self, src_path, dst_path):
        raise NotImplementedError("Rename method is not implemented.")

    @abstractmethod
    def delete(self, path, recursive=False):
        raise NotImplementedError("Delete method is not implemented.")
