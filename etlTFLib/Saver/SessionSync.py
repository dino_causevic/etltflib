from threading import Thread, Lock, Event
from time import sleep
from time import time
from etlTFLib.common.Logger import logger
from etlTFLib.common.FileSystem.HDFSHttp import HDFSHttp
from etlTFLib.common.ETLExceptions import SessionSavingFailed
from etlTFLib.common.ModelSaver.ModelSaverAbstract import ModelSaverAbstract
from etlTFLib.config import MODEL_DEFAULT_PATH, HDFS_MODEL_PATH


class SessionSync(ModelSaverAbstract):

    class SessionSyncHDFS(Thread):

        def __init__(self):
            super(SessionSync.SessionSyncHDFS, self).__init__(name=f"HDFS_Session_Model_Sync_{time()}")
            logger.debug("Session HDFS Sync Thread created.")

        def run(self):
            try:
                logger.debug("Session HDFS Sync Thread starting ...")
                HDFSHttp().upload(HDFS_MODEL_PATH, "/".join([n for n in MODEL_DEFAULT_PATH.split("/")[:-1]]),
                                  n_threads=4)
                logger.debug("Session HDFS Sync Thread finished ...")
            except Exception as ex:
                pass

    class SessionSyncThread(Thread):

        def __init__(self, session, path, close_event):
            super(SessionSync.SessionSyncThread, self).__init__(name=f"Session_Model_Sync_{time()}")
            self.session = session
            self.path = path
            self.close_event = close_event
            self.counter = 0

            logger.debug("Session Sync Thread created.")

        def _save_model(self):
            logger.debug("Saving model starting ...")

            try:
                self.session.save()
                logger.debug("Model saved.")
            except SessionSavingFailed as ex:
                logger.error("Session saving failed.")
                logger.exception(ex)
                logger.error("--------------------")
                logger.exception(ex.reason)
            except (KeyboardInterrupt, SystemExit, SystemError) as pex:
                logger.info("Process exit detected.")
                self.close_event.set()
            except Exception as uex:
                logger.exception(uex)
            finally:
                self.counter += 1
                sleep(0.25)

        def run(self):

            while not self.close_event.is_set():
                self._save_model()

            self._save_model()

    def __init__(self, session, path=MODEL_DEFAULT_PATH):
        super(SessionSync, self).__init__(session, path)

        self.lock = Lock()

        self._close = Event()
        self._close.clear()

        self.session_thread = SessionSync.SessionSyncThread(
            self, path,
            self._close
        )

        logger.debug("Session Sync created.")

    def run(self):
        self.session_thread.start()
        return self

    def stop(self):
        logger.info("Session Sync stopping ...")
        self._close.set()

        session_hdfs_sync = SessionSync.SessionSyncHDFS()
        session_hdfs_sync.start()
        session_hdfs_sync.join()
        logger.info("Session sync stopped.")

