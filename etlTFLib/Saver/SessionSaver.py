from etlTFLib.common.ModelSaver.ModelSaverAbstract import ModelSaverAbstract
from etlTFLib.config import MODEL_DEFAULT_PATH


class SessionSaver(ModelSaverAbstract):

    def __init__(self, session, path=MODEL_DEFAULT_PATH):
        super(SessionSaver, self).__init__(session, path)
