from setuptools import setup, find_packages

packages = find_packages()

setup(name='etlTFLib',
      version='0.1',
      description='ETL Lib for TF executor.',
      author='Dino Causevic',
      author_email='dincaus@gmail.com',
      include_package_data=True,
      install_requires=[
            "hdfs==2.0.16",
            "tensorflow==1.4.1",
            "pyarrow==0.8.0"
      ],
      # url='https://app.deveo.com/datazup/projects/etlpipes/repositories/VertxEventBusPy/tree/master',
      packages=find_packages())
